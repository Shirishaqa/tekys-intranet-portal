package objects;

import org.openqa.selenium.By;

public class Registration {
	
public static  By NewEmployee       = By.xpath("//a[@id='newEmployee_id']");
public static  By Name              = By.xpath("//input[@name='empName']");
public  static By EmpId             = By.xpath("//input[@name='empId']");
public static  By Email_Address     = By.xpath("//input[@name='empEmail']");
public static  By PhoneNumber       = By.xpath("//input[@name='empPhone']");
public static  By Alternate_Mobile  = By.xpath("//input[@name='empAlterPhone']");
public static  By Designation       = By.xpath("//select[@name='designation']");
public static  By Dropdown_Option   = By.xpath("//option[text()='Senior Test Engineer']");
public static  By ActualDOB         = By.xpath("//input[@id='empActualDOB_id']");
public static  By OfficialDOB       = By.xpath("//input[@name='empOfficialDOB']");
public static  By DOJ               = By.xpath("//input[@name='empDateOfJoining']");
public static  By Reportmanager     = By.xpath("//select[@name='reportingmanager']");
public static  By Emp_Pan           = By.xpath("//input[@name='empPan']");
public static  By AdharTextbox      = By.xpath("//input[@name='empAadhar']");
public static  By Upload_pan        = By.xpath("//i[@class='fa fa-cloud-upload']");
public static  By DesignationText   = By.name("Designation");
public static By  Upload_Aadhar     = By.xpath("//i[@id='adharcard_id']");
public static By  Passport           = By.xpath("//i[@id='passportphoto_id']");
public static By CurrentAddress     =  By.xpath("//textarea[@name='empCurrentAddress']");
public static By PermenantAddress   =  By.xpath("//textarea[@name='empPermanentAddress']");
public static By Save               =  By.xpath("//button[@id='newEmployeeSave_id']");
public static By  Reset             =  By.xpath("//button[@id='registerFormReset_id']");
public static By Namerequiredtext   =  By.xpath("//div[text()='Name is required.']");
public static By DesignationreqText =  By.xpath("//div[text()='Designation is required.']");
public static By CheckBox           = By.id("myCheck");


}
