package objects;

import org.openqa.selenium.By;

public class Employees_Locators {
	
	public static By Employees                     = By.id("employees_id");
	public static By Delete                        = By.id("deleteIcon_id21");
	public static By Edit                          = By.id("editIcon_id20");
	public static By EmployeeDetails               = By.xpath("//h2[text()=\" Employee Details\"]");
	public static By Update_EmployeeDetails        = By.xpath("//p-header[text()=\"Update Employee Details\"]");
	public static By Update                        = By.xpath("//button[@id='updateEmployee_id']");
	public static By Cancel                        = By.xpath("//button[@id='updateEmployeeCancel_id']");
	public static By Tekys_Id                      = By.xpath("//input[@id='empid']");
	public static By Name                          = By.xpath("//input[@id='empname']");
	public static By Search                        = By.id("searchHere_id");
	
			
	

}
