package objects;

import org.openqa.selenium.By;

public class ChangePassword_Locators {
	
	public static By Dropdown               = By.xpath("//a[@class='dropdown-toggle']");
	public static By ChangePassword         = By.xpath("//a[@id='changePassword_id']");
	public static By NewPassword            = By.xpath("//input[@name='newPassword']");
	public static By ConfirmPassword        = By.xpath("//input[@name='confirmPassword']");
	public static By Save                   = By.xpath("//button[@class='btn btn-success']");
	public static By Cancel                 = By.xpath("//button[@class='btn btn-danger']");
	public static By HeaderText             = By.xpath("//p-header[text()='Change Password']");
	public static By PasswordText           = By.xpath("//div[text()=' Password is required.']");
	public static By Password_MismatchText  = By.xpath("//div[text()='Confirm Password should match with New Password.']");
	

}
