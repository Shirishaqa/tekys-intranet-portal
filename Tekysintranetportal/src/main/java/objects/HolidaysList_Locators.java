package objects;

import org.openqa.selenium.By;

public class HolidaysList_Locators {

	public static By HolidaysList  = By.xpath("//a[@id='holidayList_id']");
	public static By HeaderText    = By.xpath("//h2[text()='Holiday List']");
	public static By Year          = By.xpath("//th[text()='Year']");
	public static By Occasion      = By.xpath("//th[text()='Occassion']");
	public static By Date          = By.xpath("//th[text()='Date']");
	public static By Isoptional    = By.xpath("//th[text()='IsOptional']");
	public static By EditHolidays  = By.xpath("//u[text()='Edit Holidays']");
	public static By Update        = By.xpath("//button[@id='holidayUpdateButton_id']");
	public static By Cancel        = By.xpath("//button[@id='holidayUpdateCancel_id']");
	public static By AddHoliday    = By.xpath("//u[text()='Add a Holiday']");
	public static By HolidayName   = By.xpath("//input[@name='newHoliday']");
	public static By Save          = By.xpath("//button[@id='holidaySaveButton_id']");
	public static By CancelText    = By.xpath("//button[@id='holidaySaveCancel_id']");
	public static By Dropdown_2019 = By.xpath("//select[@id='holidayYear']");
	public static By ErrorText     = By.xpath("//div[text()='Holiday Name is required.']");
}
