package objects;

import org.openqa.selenium.By;

public class Login
{
	public By Email                         = By.xpath("//input[@name='employeeId']");
	public By Password                      = By.xpath("//input[@name='password']");
	public By Login                         = By.xpath("//button[@type='submit']");
	public By HeaderText                    = By.xpath("//p[@class='menu-header']");
	public By LeaveApproval                 = By.xpath("//a[@id='approval_id']");
	
}
