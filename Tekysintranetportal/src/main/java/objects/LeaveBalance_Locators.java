package objects;

import org.openqa.selenium.By;

public class LeaveBalance_Locators {
	
	public static By LeaveBalance    = By.id("leaveBalance_id");
	public static By DropDown        = By.id("employeeLeaveBalance_id");
	public static By Emp_Name        = By.xpath("//span[text()='shirisha']");
	public static By YearDrop        = By.xpath("//select[@id='Id']");
	public static By HeaderText      = By.xpath("//h2[text()='Leave Balance']");
	

}
