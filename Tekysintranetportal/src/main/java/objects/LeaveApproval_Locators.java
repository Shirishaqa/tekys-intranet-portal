package objects;

import org.openqa.selenium.By;

public class LeaveApproval_Locators {
	
public static By  LeaveApproval_Button    = By.xpath("//a[@id='approval_id']");
public static By  Norecordsfound          = By.xpath("//h4[text()='No records found']");
public static By ToggleIcon               =  By.xpath("//span[@class='ui-accordion-toggle-icon pi pi-fw pi-caret-right']");
public static By EmployeeName             =  By.xpath("//label[text()='Employee Name:']");
public static By EmployeeId               =  By.xpath("//label[text()='Employee Id:']");
public static By StartDate                =  By.xpath("//label[text()='Start Date:']");
public static By EndDate                  =  By.xpath("//label[text()='End Date:']");
public static By LeavePolicy              =  By.xpath("//label[text()='Leave Type:']");
public static By NoLeaveDays              =  By.xpath("//label[text()='No Of Leave Days:']");
public static By  LeaveReason             =  By.xpath("//label[text()='Leave Reason:']");
public static By  CommentsTextbox         =  By.xpath("//textarea[@id='leaveComments43']");
public static By  Approve                 =  By.xpath("//button[@class='btn btn-success']");
public static By  Reject                  =  By.xpath("//button[@class='btn btn-danger']");
public static By HeaderText               =  By.xpath("//h2[text()='Leave Approval']");
public static By Confirmation             =  By.xpath("//span[text()='Confirmation']");
public static  By Confirmation_message    =  By.xpath("//span[text()='Are you sure you want to approve leave?']");
public static  By Yes                     =  By.xpath("//span[text()='Yes']");
public static  By No                      =  By.xpath("//span[text()='No']");
public static By  Confirmation_msg2       =  By.xpath("//span[text()='Are you sure you want to reject leave?']");
}
