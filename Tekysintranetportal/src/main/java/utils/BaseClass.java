package utils;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BaseClass
{
	public static String TimeStamp = new SimpleDateFormat("MM.dd.HH.mm.ss").format(new Date());
	static boolean b = true;
	public String TestReportspath;
	public String suiteName;
	public String TestName = this.getClass().getSimpleName();
	public String TestFullName = null;
	public String winHandleBefore = null;
	public static ExtentReports extent;
	public static ExtentTest test;

	public static Properties prop = new Properties();
	// int r;
	public static WebDriver driver = null;
	public static WebDriverWait wait;
	public static String extentReport;

	@BeforeSuite
	public void setUpSuiteDetails(ITestContext ctx) throws Exception
	{
		
		extentReport = "//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Tekysintranetportal//Reports//Reports.html"+TestName+TimeStamp+".html";
		extent = new ExtentReports(extentReport, true);
	}

	@BeforeMethod
	public void captureDesc(Method method)
	{
		test = extent.startTest(this.getClass().getSimpleName()+"::"+method.getName());
		test.assignCategory(getClass().getSimpleName());
	}

	@BeforeTest
	public void beforeTest() throws MalformedURLException
	{
		String cwd = System.getProperty("user.dir");
		File file = new File("//Users//apple/Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Tekysintranetportal//Data.properties");
		FileInputStream fileInput = null;
		try
		{
			fileInput = new FileInputStream(file);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try
		{
			prop.load(fileInput);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		/*DesiredCapabilities capabilities = DesiredCapabilities.android();
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");
		capabilities.setCapability(MobileCapabilityType.VERSION, "5.0.1");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		// Create object of AndroidDriver class and pass the url and capability
		// that we created
		driver = new AndroidDriver<WebElement>(url, capabilities);
		wait = new WebDriverWait(driver, 20);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);*/
		System.out.println("launching chrome browser");
		//System.setProperty("webdriver.chrome.driver",  "/usr/local/bin/chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("http://google.com");
		driver.get("https://www.google.com/");
		driver.get("https://tekysportalqa.azurewebsites.net");
	}
	public static String createScreenshot(WebDriver driver)
	{
		 String reportLocation = "//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Tekysintranetportal//Screenshots.html";
		 String imageLocation = "images/";
		
			 
		    UUID uuid = UUID.randomUUID();
		 
		    // generate screenshot as a file object
		    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		    try {
		        // copy file object to designated location
		        FileUtils.copyFile(scrFile, new File("//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Tekysintranetportal//Reports//Screenshots.html" + imageLocation + uuid + ".png"));
		    } catch (IOException e) {
		        System.out.println("Error while generating screenshot:\n" + e.toString());
		    }
		    return "//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Tekysintranetportal//Reports//Screenshots.html/" + uuid + ".png";
		    
		   
	}

	@AfterMethod
	public void tearDown(ITestResult result, Method method) throws IOException
	{
		if (result.getStatus()==ITestResult.SUCCESS)
		{
			String screenshot_path =  createScreenshot(driver);
			String image = test.addScreenCapture(screenshot_path);
			test.log(LogStatus.PASS, "Passed "+method.getName(), image);
		} else
		{
			String screenshot_path = createScreenshot(driver);
			String image = test.addScreenCapture(screenshot_path);
			test.log(LogStatus.FAIL, "Failed "+method.getName(), image);
		}
		extent.endTest(test);
	}

	@AfterSuite
	public void afterSuite() throws Exception
	{
		try
		{
			extent.flush();
			wait(5000);
			//driver.quit();
		}
		catch (Exception ex)
		{
			// System.out.println("Result Suite file is not being generated :
			// "+ex.getMessage());
		}
	}

	@BeforeClass
	public void BeforeClass() throws Exception
	{
		Thread.sleep(1000);
		TestName = this.getClass().getSimpleName();
		TestFullName = this.getClass().getName();
		System.out.println("TestName ::::: "+TestName);
		Thread.sleep(2000);
	}

	@AfterClass
	public void afterClass() throws Exception
	{
		/*
		 * readData.addSummaryReport(TestName,desc.get(r), TestFullName); r++;
		 * readData.knowTestCaseStatus(TestName);
		 */
		// flush();
	}

	public void openURL(String url) throws Exception
	{
		try
		{
			driver.get(url);
		}
		catch (RuntimeException localRuntimeException)
		{
			localRuntimeException.getMessage();
		}
	}

	/**
	 * This Method is used to click on an element using locator, Element is
	 * Clickable - it is Displayed and Enabled
	 */
	public void click(By locator) throws Exception
	{
		wait(2000);
		//WebElement element = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
		//element.click();
		driver.findElement(locator).click();
	}
	public void MoveToElementNclick(By locator) throws Exception
	{
		wait(2000);
		WebElement element =  driver.findElement(locator);
		Actions action = new Actions(driver);
		action.moveToElement(element).click().perform();
		wait(2000);
	}

	public static void switchToChild_Tab()
	{
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of
		// Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext())
		{
			String childWindow = itr.next();
			// Compare whether the main windows is not equal to child window. If
			// not equal, we will close.
			if (!mainWindow.equals(childWindow))
			{
				driver.switchTo().window(childWindow);
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver.switchTo().window(mainWindow);
				// driver.close();
				driver.switchTo().window(childWindow);
			}
		}
	}

	public static boolean ClicIfElementPresent(By loc)
	{
		if (driver.findElement((loc)).isDisplayed())
		{
			String text = (driver.findElement((loc)).getText());
			System.out.println("Available Element is: "+text);
			driver.findElement(loc).click();
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * This Method is used to clear if any data found in the text box and pass
	 * the required data
	 */
	public void type(By locator, String data) throws Exception
	{
		try
		{
//			wait(2000);
//			WebElement element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
			// Scroll the browser to the element's X position
			// ((JavascriptExecutor)
			// driver).executeScript("window.scrollTo(0,"+elementToClick.getLocation().x+")");
			// Click the element
//			Actions action = new Actions(driver);
//			action.moveToElement(element).click().perform();
//			wait(2000);
//			driver.findElement(locator).clear();
//			wait(2000);
			driver.findElement(locator).sendKeys(data);
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in entering the text in element:"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
			throw localRuntimeException;
		}
	}
	public void CheckBox(By locator) {
	
	WebElement oCheckBox = driver.findElement(locator);
	oCheckBox.click();
	}
	
	public void Clear(By locator) {
		driver.findElement(locator).clear();
		wait(2000);
	}
	
	public String Time()
	{
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("hhmmssaa");  
	    return formatter.format(date);  
	}

	public void select(By locator, String data) throws Exception
	{
		try
		{
			Select s = new Select(driver.findElement(locator));
			s.selectByVisibleText(data);
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in selecting value from dropdown:"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
		}
	}

	public void select(By locator, int no) throws Exception
	{
		try
		{
			Select dropdown = new Select(driver.findElement(locator));
			dropdown.selectByIndex(no);
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in selecting value from dropdown:"+localRuntimeException.getMessage()+"Fail");
		}
	}

	
	public void highlight(By locator) throws Exception
	{
		try
		{
			WebElement elem = driver.findElement(locator);
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].style.border='3px solid green',background='yellow'", elem);
			String text = elem.getText();
			System.out.println("Highlighted text is :"+text);
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in Highlighting the element :"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
			throw localRuntimeException;
		}
	}

	public void highlightbyWebElement(By locator) throws Exception
	{
		WebElement elem = driver.findElement(locator);
		try
		{
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].style.border='3px solid blue'", elem);
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in Highlighting the element :"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
		}
	}

	public void waitForElement(By locator, int timer) throws Exception
	{
		try
		{
			for (int i = 0; i<timer; i++)
			{
				try
				{
					driver.findElement(locator).isDisplayed();
					System.out.println("Element is available :"+locator);
					break;
				}
				catch (RuntimeException localRuntimeException)
				{
					Thread.sleep(1000);
					System.out.println("Waiting for........"+locator);
				}
			}
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in performing Wait:"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
		}
	}

	public static boolean IsElementPresent(String loc)
	{
		if (driver.findElement(By.xpath(loc)).isDisplayed())
		{
			return true;
		} else
		{
			return false;
		}
	}

	public int totalitemsdropdownlist(WebElement elem)
	{
		List<WebElement> dropdown_values = null;
		try
		{
			Select dropdownfield = new Select(elem);
			dropdown_values = dropdownfield.getOptions();
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in finding total no. of elements in dropdown: "+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
		}
		return dropdown_values.size();
	}

	public static void verifyElementIsEnabled(WebElement elem, boolean paramBoolean)
	{
		try
		{
			boolean bool = elem.isEnabled();
			if (bool==paramBoolean)
				System.out.println("Element is present in expected state"+elem+"Pass");
			else
				System.out.println("Element is not present in expected state"+elem+"Fail");
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Element not found:"+elem+"Fail");
			localRuntimeException.getMessage();
		}
	}

	public boolean isAlertPresent()
	{
		boolean foundAlert = false;
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 60L);
			wait.until(ExpectedConditions.alertIsPresent());
			foundAlert = true;
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in finding Alert Is Present:Fail");
			localRuntimeException.getMessage();
		}
		return foundAlert;
	}

	public void handleConfirmation(String paramString)
	{
		Alert localAlert = driver.switchTo().alert();
		if (localAlert!=null)
		{
			if (paramString.trim().equalsIgnoreCase("Y"))
			{
				System.out.println("Alert accepted!!!");
				localAlert.accept();
			} else if (paramString.trim().equalsIgnoreCase("N"))
			{
				System.out.println("Alert Rejected!!!");
				localAlert.dismiss();
			}
		} else
		{
			System.out.println("Error in finding Alert:Fail");
		}
	}

	public String getAlertMessageText()
	{
		String str1 = null;
		try
		{
			str1 = driver.switchTo().alert().getText();
			return str1;
		}
		catch (Exception e)
		{
		}
		return str1;
	}

	public static void sleep(float paramFloat)
	{
		try
		{
			long l1 = (long) (paramFloat*1000.0F);
			long l2 = System.currentTimeMillis();
			while (l2+l1>=System.currentTimeMillis())
				;
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public int getTableRowCount(String tableid)
	{
		int iRowCount = 0;
		try
		{
			List<WebElement> iRows = driver.findElements(By.xpath("//table[@id='"+tableid+"']/tbody/tr"));
			iRowCount = iRows.size();
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in fetching no. of rows:"+tableid+"Fail");
			localRuntimeException.getMessage();
		}
		return iRowCount;
	}

	public void pressEnterKey()
	{
		try
		{
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public void VerifyText(By locator, String paramString2)
	{
		WebElement elem = driver.findElement(locator);
		try
		{
			String selectedOption = elem.getText();
			if (selectedOption.trim().equalsIgnoreCase(paramString2))
			{
				System.out.println("Text was found :"+paramString2+" :Pass");
			} else
			{
				System.out.println("Text was not found :"+paramString2+" :Fail");
			}
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Element was not found :"+elem+" :Fail");
			localRuntimeException.getMessage();
			throw localRuntimeException;
		}
	}

	public String getToolTipText(WebElement elem, String paramString1)
	{
		String tooltip = null;
		try
		{
			if (elem!=null)
			{
				tooltip = elem.getAttribute(paramString1);
			}
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in Getting tool tip text:"+localRuntimeException.getMessage()+"Fail");
			localRuntimeException.getMessage();
		}
		return tooltip;
	}

	public static void verifyListItems(WebElement elem)
	{
		try
		{
			Select listBox = new Select(elem);
			List<WebElement> allItems = listBox.getOptions();
			for (WebElement item : allItems)
			{
				System.out.println("Item is available in list:"+item);
			}
		}
		catch (Exception e)
		{
			System.out.println("Issue While Selecting Value in Drop Down Object :"+elem);
		}
	}

	public void getElementLocation(By locator)
	{
		WebElement element = driver.findElement(locator);
		element.getLocation();
	}

	public static By getLocators(String paramString1, String paramString2)
	{
		if (paramString1.trim().equalsIgnoreCase("xpath"))
			return By.xpath(paramString2);
		if (paramString1.trim().equalsIgnoreCase("cssselector"))
			return By.cssSelector(paramString2);
		if (paramString1.trim().equalsIgnoreCase("tagname"))
			return By.tagName(paramString2);
		if (paramString1.trim().equalsIgnoreCase("id"))
			return By.id(paramString2);
		if (paramString1.trim().equalsIgnoreCase("name"))
			return By.name(paramString2);
		if (paramString1.trim().equalsIgnoreCase("linktext"))
			return By.linkText(paramString2);
		return null;
	}

	public static String defaultdropdownselecteditem(WebElement elem)
	{
		Select dropdownfield = new Select(elem);
		String text = dropdownfield.getFirstSelectedOption().getText();
		System.out.println(text.trim());
		return dropdownfield.getFirstSelectedOption().getText().trim();
	}

	public void alldropdownlistvalues(By locator)
	{
		WebElement elem = driver.findElement(locator);
		Select dropdownfield = new Select(elem);
		List<WebElement> dropdownfield_values = dropdownfield.getOptions();
		System.out.println("Available dropdown values are::: ");
		for (int i = 0; i<dropdownfield_values.size(); i++)
		{
			String currentvalue = dropdownfield_values.get(i).getText();
			// allvalues = concatvalue;
			System.out.println(currentvalue);
		}
		// return //allvalues.substring(0, allvalues.length() - 1);
	}

	public String getdate(String format)
	{
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date d = new Date();
		String date = df.format(d);
		System.out.println(date);
		return date;
	}

	public static String getattributevalue(By locator, String requiredattribute) throws Exception
	{
		String attribute = null;
		try
		{
			attribute = driver.findElement(locator).getAttribute(requiredattribute);
			System.out.println(attribute);
		}
		catch (RuntimeException localRuntimeException)
		{
		}
		return attribute;
	}

	public void alertaction(String action)
	{
		try
		{
			if (action.equals("ok"))
			{
				driver.switchTo().alert().accept();
			} else if (action.equals("cancel"))
			{
				driver.switchTo().alert().dismiss();
			}
		}
		catch (Exception e)
		{
			System.out.println("Error in performing action on Alert box:"+action+"Fail");
		}
	}

	public String printText(By locator)
	{
		String text = driver.findElement(locator).getText();
		System.out.println("The text is :"+text);
		return text;
	}

	public void scrollingToElemento(By locator)
	{
		WebElement element = driver.findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	public int totallinnks(WebElement elem)
	{
		return elem.findElements(By.tagName("a")).size();
	}

	public void capturesnapshot(String destinationPath) throws IOException
	{
		try
		{
			File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(f, new File(destinationPath));
		}
		catch (Exception e)
		{
			System.out.println("Error in Capturing Screenshot:Fail");
		}
	}

	public void dragAndDrop(By question, By target)
	{
		WebElement e1 = driver.findElement(question);
		WebElement e2 = driver.findElement(target);
		Actions a = new Actions(driver);
		a.dragAndDrop(e1, e2).build().perform();
	}

	public boolean verifyElementExist(By locator)
	{
		boolean blnStatus = false;
		WebDriverWait localWebDriverWait = new WebDriverWait(driver, 60L);
		WebElement elem = driver.findElement(locator);
		try
		{
			localWebDriverWait.until(ExpectedConditions.presenceOfElementLocated((By) elem));
			System.out.println("Element is available:"+elem+"Pass");
			blnStatus = true;
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in finding Element:"+localRuntimeException.getMessage()+"Pass");
		}
		return blnStatus;
	}

	public void Mousehover(WebElement elem)
	{
		try
		{
			Actions action = new Actions(driver);
			action.moveToElement(elem).build().perform();
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Error in Hover on element"+localRuntimeException.getMessage()+"Pass");
		}
	}

	public void selectListItem(WebElement elem, String paramString)
	{
		int i = 0;
		try
		{
			List localList = driver.findElements(By.tagName("option"));
			Iterator localIterator = localList.iterator();
			while (localIterator.hasNext())
			{
				WebElement localWebElement2 = (WebElement) localIterator.next();
				if (paramString.trim().equalsIgnoreCase(localWebElement2.getText().trim()))
				{
					i = 1;
					localWebElement2.click();
					break;
				}
			}
			System.out.println("Selected option:"+paramString+"Successfully"+"Pass");
			if (i==0)
			{
				System.out.println("Selected option:"+paramString+"is not present"+"Fail");
			}
		}
		catch (RuntimeException localRuntimeException)
		{
			System.out.println("Issue while Selected value:"+localRuntimeException.getMessage()+"is not present"+"Fail");
		}
	}

	public void wait(int ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch (InterruptedException e)
		{
			System.out.println(e.getMessage());
			;
		}
	}
	public String GetText(By locators)
	{
		String Text = driver.findElement(locators).getText();
		return Text;
	}
	public void getinformation(By locators, String value1,String value2)
	{
		String Location_Name = GetText(locators);
		Assert.assertEquals(Location_Name,value1,value2);
		System.out.println(Location_Name);
	}
	
	public void Scroll() {
	JavascriptExecutor js = (JavascriptExecutor)driver;
	 js.executeScript("scroll(0,350)");
	}
	
	public void DropDown(String name, String text) {
		Select drpCountry = new Select(driver.findElement(By.name(name)));
		drpCountry.selectByVisibleText(text);
	}

	
	public void DropDown1(By locator, String text) {
		Select drpCountry = new Select(driver.findElement(locator));
		drpCountry.selectByVisibleText(text);
	}

	public void switchToBrowser(String paramString)
	{
		try
		{
			winHandleBefore = driver.getWindowHandle();
			String str1 = paramString;
			int i = 0;
			Iterator localIterator = driver.getWindowHandles().iterator();
			while (localIterator.hasNext())
			{
				String str2 = (String) localIterator.next();
				if (driver.switchTo().window(str2).getTitle().equalsIgnoreCase(str1.trim()))
				{
					i = 1;
					driver.switchTo().window(str2);
				} else
				{
					driver.switchTo().window(winHandleBefore);
				}
			}
			if (i==0)
				System.out.println("The Browser Window with title : "+str1+" is not found");
		}
		catch (Exception e)
		{
			System.out.println("Error in switching to browser"+e.getMessage());
		}
	}
	
	public void Upload_Functionality(String path) throws Exception {
		
		File file = new File(path); 
	    StringSelection StringSelection = new StringSelection(file.getAbsolutePath());
	  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(StringSelection, null);

	  driver.switchTo().window(driver.getWindowHandle());

	Robot robot = new Robot();

	//Open Goto window
	robot.keyPress(KeyEvent.VK_META);
	robot.keyPress(KeyEvent.VK_SHIFT);
	robot.keyPress(KeyEvent.VK_G);
	robot.keyRelease(KeyEvent.VK_META);
	robot.keyRelease(KeyEvent.VK_SHIFT);
	robot.keyRelease(KeyEvent.VK_G);

	//Paste the clipboard value
	robot.keyPress(KeyEvent.VK_META);
	robot.keyPress(KeyEvent.VK_V);
	robot.keyRelease(KeyEvent.VK_META);
	robot.keyRelease(KeyEvent.VK_V);

	//Press Enter key to close the Goto window and Upload window
	robot.delay(1000);
	robot.keyPress(KeyEvent.VK_ENTER);
	robot.keyRelease(KeyEvent.VK_ENTER);
	robot.delay(1000);
	robot.keyPress(KeyEvent.VK_ENTER);
	robot.keyRelease(KeyEvent.VK_ENTER);
		}
	public void Tables_Rows() {
		int rowCount=driver.findElements(By.tagName("tr")).size();

        //Get Column Count
        int colCount=driver.findElements(By.xpath("//thead//th")).size();

        System.out.println("Row count :" + rowCount);
        System.out.println("Col count :" + colCount);

        //Print table Data

        for(WebElement tdata:driver.findElements(By.tagName("tr")))
        {
        System.out.println(tdata.getText());
	}
	}
}
