package testMethods;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import objects.Login;
import utils.BaseClass;
 

public class IntranetportalSteps extends BaseClass
{
	Login loginLoc = new Login();
	

	public void FNP_PD_514(String Email, String Pass, String Torder) throws Exception
	{
		try
		{
			
			String screenshot_path = createScreenshot(driver);
			String image = test.addScreenCapture(screenshot_path);
			test.log(LogStatus.PASS, "Verified Track order option availability", image);
			wait(2000);
			type(loginLoc.Email, Email);
			wait(2000);
			type(loginLoc.Password, Pass);
			wait(2000);
			//hideKeyboard();
			//click(loginLoc.Continue);
			wait(2000);
			wait(2000);
		}
		catch (Exception e)
		{
			throw e;
		}
	}

	

//	public void FNP_PD_516(String reTry) throws Exception
//	{
//		try
//		{
//			Boolean ele = driver.findElements(By.xpath("//h5[text()=' MY ORDER']//following::button[1]")).size()!=0;
//			if (ele==true)
//			{
//				VerifyText(orderLoc.Retry, reTry);
//				Thread.sleep(3000);
//			} else
//			{
//				printText(orderLoc.Remove);
//			}
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//	}
//
//	public void FNP_PD_517() throws Exception
//	{
//		try
//		{
//			Thread.sleep(5000);
//			try
//			{
//				List<WebElement> Retry = driver.findElements(By.xpath("//button[text()='Retry']"));
//				System.out.println("Retry buttons available ="+Retry.size());
//				wait(2000);
//				Retry.get(4).click();
//			}
//			catch (Exception e)
//			{
//				click(orderLoc.Retry);
//			}
//			Thread.sleep(4000);
//			printText(orderLoc.amount);
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//	}
//
//	public void FNP_PD_518() throws Exception
//	{
//		try
//		{
//			click(orderLoc.backArrow);
//			wait(2000);
//			click(orderLoc.menuOption);
//			wait(2000);
//			click(orderLoc.TrackOrderMenu);
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//	}
//
//	public void FNP_PD_519() throws Exception
//	{
//		try
//		{
//			wait(2000);
//			click(orderLoc.Retry);
//			wait(2000);
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//	}
//
//	public void FNP_PD_520() throws Exception
//	{
//		try
//		{
//			click(orderLoc.backArrow);
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			throw e;
//		}
//	}
}
