package tekysportalTestSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import objects.HolidaysList_Locators;
import objects.LeaveApproval_Locators;
import objects.Registration;
import utils.BaseClass;

public class HolidayList extends BaseClass {
	Registration            Employee     = new Registration();
	WebLaunch               Webloagin    = new WebLaunch();
	LeaveApproval_Locators  Approval     = new LeaveApproval_Locators();
	HolidaysList_Locators   Holiday      = new HolidaysList_Locators();
	
	
	@BeforeTest
	public void beforetest() throws Exception {
		
		Webloagin.launch();
	}
	
	@Test(priority=1)
	public void HolidayListscreen_Navigation() throws Exception {
		click(Holiday.HolidaysList);
		Thread.sleep(2000);
		Tables_Rows();
		Thread.sleep(2000);
		
}
	@Test(priority=2)
	public void Holidays_In_2019() throws Exception {
		
		select(Holiday.Dropdown_2019, "2019");
		Thread.sleep(2000);
		Tables_Rows();
	}
	@Test(priority=3)
	public void AddaHoliday() throws Exception {
		click(Holiday.AddHoliday);
		type(Holiday.HolidayName,"testing");
		Thread.sleep(4000);
		click(Holiday.Save);
		Thread.sleep(4000);
		
	}
	
	@Test(priority=4)
	public void CancelHoliday() throws Exception {
		click(Holiday.AddHoliday);
		type(Holiday.HolidayName,"testing");
		Thread.sleep(2000);
		click(Holiday.CancelText);
		Thread.sleep(4000);	
	}
	
	@Test(priority=5)
	public void EditHoliday() throws Exception {   
		click(Holiday.EditHolidays);
		Thread.sleep(2000);
        click(Holiday.HolidayName);
		Clear(Holiday.HolidayName);
		Thread.sleep(2000);
		getinformation(Holiday.ErrorText,"Holiday Name is required.","Pass");
		type(Holiday.HolidayName,"Testing");
		Thread.sleep(4000);	
		click(Holiday.Update);
		Thread.sleep(6000);
	}
	}

