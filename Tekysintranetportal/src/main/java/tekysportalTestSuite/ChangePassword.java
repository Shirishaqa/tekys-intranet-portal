package tekysportalTestSuite;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objects.ChangePassword_Locators;
import objects.Employees_Locators;
import objects.LeaveApproval_Locators;
import objects.Registration;
import utils.BaseClass;

public class ChangePassword  extends BaseClass{
	Registration       Employee     = new Registration();
	WebLaunch          Webloagin    = new WebLaunch();
	Employees_Locators Emp          = new Employees_Locators();
	LeaveApproval_Locators  Approval     = new LeaveApproval_Locators();
	ChangePassword_Locators Password    = new ChangePassword_Locators();
	
	@BeforeClass
	public void beforeclass() throws Exception {
		Webloagin.launch();
		
	}
	
	@Test(priority=1)
	public void Navigation() throws Exception {
		
		click(Password.Dropdown);
		Thread.sleep(3000);
		click(Password.ChangePassword);
		getinformation(Password.HeaderText,"Change Password","Pass");
		type(Password.NewPassword,"Test26");
		type(Password.ConfirmPassword,"Test@26");
		click(Password.Cancel);
		
	}
	
	@Test(priority=2)
	public void Withoutentering_Save() throws Exception {
		click(Password.Dropdown);
		Thread.sleep(3000);
		click(Password.ChangePassword);
		click(Password.Save);	
		getinformation(Password.PasswordText,"Password is required.","Pass");
		Thread.sleep(3000);
	}

	@Test(priority=3)
	public void Passwordmismatch() throws Exception {
		
		type(Password.NewPassword,"Test60");
		type(Password.ConfirmPassword,"Test@16");
		click(Password.Save);	
		getinformation(Password.Password_MismatchText,"Confirm Password should match with New Password.","Pass");
	}

	
	
	@Test(priority=4)
	public void Save_Password() throws Exception {
		Clear(Password.NewPassword);
		Clear(Password.ConfirmPassword);
//		String password = "shirisha"+Time()+"12";
//		type(Employee.Email_Address,password);
		type(Password.NewPassword,"admin@123");
		type(Password.ConfirmPassword,"admin@123");
		click(Password.Save);
		
	}
	
	
}
