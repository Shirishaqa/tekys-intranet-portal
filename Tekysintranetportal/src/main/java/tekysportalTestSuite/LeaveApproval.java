package tekysportalTestSuite;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import objects.LeaveApproval_Locators;
import objects.Registration;
import utils.BaseClass;

public class LeaveApproval extends BaseClass {
	
	Registration            Employee     = new Registration();
	WebLaunch               Webloagin    = new WebLaunch();
	LeaveApproval_Locators  Approval     = new LeaveApproval_Locators();
	
	
	@BeforeTest
	public void beforetest() throws Exception {
		
		Webloagin.launch();
	}
	
	@Test(priority=1)
	public void Cancel_Approval() throws Exception {
		click(Approval.LeaveApproval_Button);
		Thread.sleep(2000);
		getinformation(Approval.HeaderText,"Leave Approval","Pass");
		click(Approval.ToggleIcon);
		Thread.sleep(2000);
		getinformation(Approval.EmployeeName,"Employee Name:","Pass");
		getinformation(Approval.EmployeeId,"Employee Id:","Pass");
		getinformation(Approval.StartDate,"Start Date:","Pass");
		getinformation(Approval.EndDate,"End Date:","Pass");
		getinformation(Approval.LeavePolicy,"Leave Type:","Pass");
		getinformation(Approval.NoLeaveDays,"No Of Leave Days:","Pass");
		getinformation(Approval.LeaveReason,"Leave Reason:","Pass");
		Thread.sleep(3000);
		//type(Approval.CommentsTextbox,"Approved");
		click(Approval.Approve);
		getinformation(Approval.Confirmation,"Confirmation","Pass");
		getinformation(Approval.Confirmation_message,"Are you sure you want to approve leave?","Pass");
		getinformation(Approval.Yes,"Yes","Pass");
		getinformation(Approval.No,"No","Pass");
		click(Approval.No);
		
	}
	
	@Test(priority=2)
	public void Approve_Approval() throws Exception {
		click(Approval.Approve);
		getinformation(Approval.Confirmation,"Confirmation","Pass");
		getinformation(Approval.Confirmation_message,"Are you sure you want to approve leave?","Pass");
		getinformation(Approval.Yes,"Yes","Pass");
		getinformation(Approval.No,"No","Pass");
		click(Approval.Yes);
		Thread.sleep(8000);
		
	}

	
	
	@Test(priority=3)
	public void Reject_Approval() throws Exception {
		click(Approval.ToggleIcon);
		Thread.sleep(2000);
		getinformation(Approval.EmployeeName,"Employee Name:","Pass");
		getinformation(Approval.EmployeeId,"Employee Id:","Pass");
		getinformation(Approval.StartDate,"Start Date:","Pass");
		getinformation(Approval.EndDate,"End Date:","Pass");
		getinformation(Approval.LeavePolicy,"Leave Type:","Pass");
		getinformation(Approval.NoLeaveDays,"No Of Leave Days:","Pass");
		getinformation(Approval.LeaveReason,"Leave Reason:","Pass");
		Thread.sleep(3000);
		click(Approval.Reject);
		Thread.sleep(3000);
		type(Approval.CommentsTextbox,"due to heavy work we rejected your leave application");
		click(Approval.Reject);
		click(Approval.Approve);
		getinformation(Approval.Confirmation,"Confirmation","Pass");
		getinformation(Approval.Confirmation_msg2,"Are you sure you want to reject leave?","Pass");
		getinformation(Approval.Yes,"Yes","Pass");
		getinformation(Approval.No,"No","Pass");
		click(Approval.Yes);
		Thread.sleep(8000);
		
		
		
		
	}
	@Test(priority=4)
	public void Verify_Empty_recordsfnctionality() throws Exception {
		
		DropDown("Id","shirisha");	
		Thread.sleep(2000);
		getinformation(Approval.Norecordsfound,"No records found","Pass");
	}
	
}
