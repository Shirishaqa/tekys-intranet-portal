package tekysportalTestSuite;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;



import objects.Registration;
import utils.BaseClass;

public class NewEmployee extends BaseClass {

	Registration Employee     = new Registration();
	WebLaunch    Webloagin    = new WebLaunch();
	
	
	

	@Test(priority=1)
	public void EmployeeRegistartion() throws Exception {
		
		click(Employee.NewEmployee);
		Thread.sleep(6000);
		type(Employee.Name,"Shirisha");
		type(Employee.EmpId,"12");
     	String Email = "shirisha"+Time()+"@tekyslab.com";
		type(Employee.Email_Address,Email);
		Thread.sleep(2000);
		type(Employee.PhoneNumber,"91234567890");
	    type(Employee.Alternate_Mobile,"1234567890");
		Thread.sleep(5000);
		DropDown("designation","Senior Test Engineer");
		type(Employee.ActualDOB,"09/12/2018");
		type(Employee.OfficialDOB,"11/02/2018");
		type(Employee.DOJ,"09/05/2018");
		Scroll();
		DropDown("reportingmanager","santhosh");
		Scroll();
		Thread.sleep(2000);
		type(Employee.Emp_Pan,"abdccc1233");
		type(Employee.AdharTextbox,"123455566666666");
		Scroll();
		DropDown("leaveapprover","shirisha");
		type(Employee.CurrentAddress,"Ameerpet"+"\n");
		type(Employee.CurrentAddress,"Hyderabad");
		type(Employee.PermenantAddress,"Tekyslab pvt ltd"+"\n");
		type(Employee.PermenantAddress,"Madhapur");
		
	}
	

	@Test(priority=2)
    public void uploadFileWithRobot() throws Exception {
    	click(Employee.Upload_pan);
		Thread.sleep(8000);
		Upload_Functionality("//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//flower.jpeg");
    }
	
	@Test(priority=3)
    public void Upload_Aadhar() throws Exception {
		
		Thread.sleep(6000);
    	    click(Employee.Upload_Aadhar);
		Thread.sleep(8000);
		Upload_Functionality("//Users//apple//Desktop//download.jpeg");
    }
	
	@Test(priority=4)
    public void PassportSizephoto() throws Exception {
    	Thread.sleep(8000);
    	click(Employee.Passport);
		Thread.sleep(8000);
		Upload_Functionality("//Users//apple//Documents//AutomationTestProjects//TekysintranetPortal//tekys-intranet-portal//Roses.jpeg");
    }
	
	@Test(priority=5)
	public void Save_Reset() throws Exception {
		Thread.sleep(6000);
		click(Employee.Save);
		Thread.sleep(6000);
	}
	
	@Test(priority=6)
	public void Reset_Functionality() throws Exception {
		type(Employee.Name,"Shirisha");
		type(Employee.EmpId,"12");
     	String Email = "shirisha"+Time()+"@tekyslab.com";
		type(Employee.Email_Address,Email);
		Thread.sleep(2000);
		type(Employee.PhoneNumber,"91234567890");
	    type(Employee.Alternate_Mobile,"1234567890");
		Thread.sleep(5000);
		DropDown("designation","Senior Test Engineer");
		type(Employee.ActualDOB,"09/12/2018");
		type(Employee.OfficialDOB,"11/02/2018");
		type(Employee.DOJ,"09/05/2018");
		Scroll();
		DropDown("reportingmanager","santhosh");
		Scroll();
		type(Employee.Emp_Pan,"abdccc1233");
		type(Employee.AdharTextbox,"123455566666666");
		Scroll();
		DropDown("leaveapprover","shirisha");
		type(Employee.CurrentAddress,"Ameerpet"+"\n");
		type(Employee.CurrentAddress,"Hyderabad");
		type(Employee.PermenantAddress,"Tekyslab pvt ltd"+"\n");
		type(Employee.PermenantAddress,"Madhapur");
		click(Employee.Reset);
	}
	
	@Test(priority=6)
	public void Withoutname_Savefunctionality() throws Exception {
		type(Employee.EmpId,"12");
     	String Email = "shirisha"+Time()+"@tekyslab.com";
		type(Employee.Email_Address,Email);
		Thread.sleep(2000);
		type(Employee.PhoneNumber,"91234567890");
	    type(Employee.Alternate_Mobile,"1234567890");
		Thread.sleep(5000);
		DropDown("designation","Senior Test Engineer");
		type(Employee.ActualDOB,"09/12/2018");
		type(Employee.OfficialDOB,"11/02/2018");
		type(Employee.DOJ,"09/05/2018");
		Scroll();
		DropDown("reportingmanager","santhosh");
		Scroll();
		type(Employee.Emp_Pan,"abdccc1233");
		type(Employee.AdharTextbox,"123455566666666");
		Scroll();
		DropDown("leaveapprover","shirisha");
		type(Employee.CurrentAddress,"Ameerpet"+"\n");
		type(Employee.CurrentAddress,"Hyderabad");
		type(Employee.PermenantAddress,"Tekyslab pvt ltd"+"\n");
		type(Employee.PermenantAddress,"Madhapur");
		click(Employee.Save);
		Scroll();
		getinformation(Employee.Namerequiredtext,"Name is required.","Pass");
		click(Employee.Reset);
		Scroll();
	}
	@Test(priority=7)
	public void WithoutDesignation_Savefunctionality() throws Exception {
		
		type(Employee.Name,"Shirisha");
		type(Employee.EmpId,"12");
     	String Email = "shirisha"+Time()+"@tekyslab.com";
		type(Employee.Email_Address,Email);
		Thread.sleep(2000);
		type(Employee.PhoneNumber,"91234567890");
		type(Employee.ActualDOB,"09/12/2018");
		type(Employee.OfficialDOB,"11/02/2018");
		type(Employee.DOJ,"09/05/2018");
		Thread.sleep(5000);
		Scroll();
		type(Employee.CurrentAddress,"Ameerpet"+"\n");
		type(Employee.CurrentAddress,"Hyderabad");
		type(Employee.PermenantAddress,"Tekyslab pvt ltd"+"\n");
		type(Employee.PermenantAddress,"Madhapur");
		click(Employee.Save);
		getinformation(Employee.DesignationreqText,"Designation is required.","Pass");
		Scroll();
		click(Employee.Reset);	
		
	}
	
	@Test(priority=8)
	public void AddressCheckbox_Functionality() throws Exception {
		
		click(Employee.NewEmployee);
		Thread.sleep(6000);
		type(Employee.Name,"Shirisha");
		type(Employee.EmpId,"12");
     	String Email = "shirisha"+Time()+"@tekyslab.com";
		type(Employee.Email_Address,Email);
		Thread.sleep(2000);
		type(Employee.PhoneNumber,"91234567890");
	    type(Employee.Alternate_Mobile,"1234567890");
		Thread.sleep(5000);
		DropDown("designation","Senior Test Engineer");
		type(Employee.ActualDOB,"09/12/2018");
		type(Employee.OfficialDOB,"11/02/2018");
		type(Employee.DOJ,"09/05/2018");
		Scroll();
		DropDown("reportingmanager","santhosh");
		Scroll();
		Thread.sleep(2000);
		type(Employee.Emp_Pan,"abdccc1233");
		type(Employee.AdharTextbox,"123455566666666");
		Scroll();
		DropDown("leaveapprover","shirisha");
		type(Employee.CurrentAddress,"Ameerpet"+"\n");
		type(Employee.CurrentAddress,"Hyderabad");
		CheckBox(Employee.CheckBox);
		
	}
		
	

@org.testng.annotations.BeforeClass
public void beforeclass() throws Exception {
	Webloagin.launch();
}

}


