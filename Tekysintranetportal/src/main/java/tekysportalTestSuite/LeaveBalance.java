package tekysportalTestSuite;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objects.Employees_Locators;
import objects.LeaveApproval_Locators;
import objects.LeaveBalance_Locators;
import objects.LeaveHistory_Locators;
import objects.Registration;
import utils.BaseClass;

public class LeaveBalance extends BaseClass {
	
	Registration       Employee     = new Registration();
	WebLaunch          Webloagin    = new WebLaunch();
	Employees_Locators Emp          = new Employees_Locators();
	LeaveApproval_Locators  Approval   = new LeaveApproval_Locators();
	LeaveBalance_Locators   Balance            = new LeaveBalance_Locators();
	LeaveHistory_Locators history      = new LeaveHistory_Locators();
	

	
	@BeforeClass
	public void beforeclass() throws Exception {
		Webloagin.launch();
		
	}
	
	@Test(priority=1)
	public void Navigation() throws Exception {
		click(Balance.LeaveBalance);
		Thread.sleep(3000);
		getinformation(Balance.HeaderText,"Leave Balance","Pass");
		Tables_Rows();
		Thread.sleep(2000);
		
	}
	@Test(priority=2)
	public void Table_Row() throws Exception {
		DropDown1(Balance.DropDown,"shirisha");
		Thread.sleep(3000);
		getinformation(Balance.Emp_Name,"shirisha","Pass");
		Tables_Rows();
		Thread.sleep(2000);
		
	}
	@Test(priority=3)
	public void Year() throws Exception {
		DropDown1(Balance.YearDrop,"2018");
		Tables_Rows();
		Thread.sleep(2000);
		
	}
	
	
}
