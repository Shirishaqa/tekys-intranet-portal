package tekysportalTestSuite;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objects.Employees_Locators;
import objects.LeaveApproval_Locators;
import objects.LeaveHistory_Locators;
import objects.Registration;
import utils.BaseClass;

public class LeaveHistory extends BaseClass {

	Registration       Employee     = new Registration();
	WebLaunch          Webloagin    = new WebLaunch();
	Employees_Locators Emp          = new Employees_Locators();
	LeaveApproval_Locators  Approval     = new LeaveApproval_Locators();
	LeaveHistory_Locators history      = new LeaveHistory_Locators();
	
	@BeforeClass
	public void beforeclass() throws Exception {
		Webloagin.launch();
		
	}
	
	@Test(priority=1)
	public void Navigation_LeaveHistory() throws Exception {
		click(history.LeaveHistory);
		Thread.sleep(3000);
		getinformation(history.HeaderText,"Leave History","Pass");
		Tables_Rows();
		Thread.sleep(2000);
	}
	
	@Test(priority=2)
	public void SelectName_Dropdown() throws Exception {
		DropDown("Id","shirisha");
		Tables_Rows();
		Thread.sleep(2000);
	}
	
	@Test(priority=3)
	public void SelectYear_Dropdown() throws Exception {
		DropDown("id","All");
		Tables_Rows();
		Thread.sleep(2000);
	}
	@Test(priority=4)
	public void Combination_year_Name() throws Exception {
		DropDown("Id","All");
		Thread.sleep(2000);
		DropDown("id","2018");
		Tables_Rows();
		
	}
}
