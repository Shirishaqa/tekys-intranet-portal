package tekysportalTestSuite;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objects.Employees_Locators;
import objects.LeaveApproval_Locators;
import objects.Registration;
import utils.BaseClass;

public class Employees extends BaseClass {

	Registration       Employee     = new Registration();
	WebLaunch          Webloagin    = new WebLaunch();
	Employees_Locators Emp          = new Employees_Locators();
	LeaveApproval_Locators  Approval     = new LeaveApproval_Locators();
	
	@BeforeClass
	public void beforeclass() throws Exception {
		Webloagin.launch();
		
	}
	
	@Test(priority=1)
	public void EmployeeScreen_navigation() throws Exception {
		click(Emp.Employees);
		getinformation(Emp.EmployeeDetails,"Employee Details","Pass");
		Tables_Rows();
		Thread.sleep(3000);
	}
	
	//@Test(priority=2)
	public void Edit_Functionality() throws Exception {
	click(Emp.Edit);
	Thread.sleep(2000);
	Scroll();
	Clear(Emp.Tekys_Id);
	type(Emp.Tekys_Id,"16");
	click(Emp.Cancel);
	
	}
	
	@Test(priority=3)
	public void Update_Functionality_Edit() throws Exception {
		click(Emp.Edit);
		Thread.sleep(2000);
		Scroll();
		Clear(Emp.Name);
		type(Emp.Name,"shirisha");
		click(Emp.Update);
		System.out.println("Employee updated successfully");
		Thread.sleep(2000);
	}
	@Test(priority=4)
	public void Delete_Functonality_No() throws Exception {
		click(Emp.Delete);
		getinformation(Approval.Confirmation,"Confirmation","Pass");
		getinformation(Approval.Yes,"Yes","Pass");
		getinformation(Approval.No,"No","Pass");
		click(Approval.No);	
		
	}
	@Test(priority=5)
	public void Delete_Functonality_Yes() throws Exception {
		click(Emp.Delete);
		getinformation(Approval.Confirmation,"Confirmation","Pass");
		getinformation(Approval.Yes,"Yes","Pass");
		getinformation(Approval.No,"No","Pass");
		click(Approval.Yes);	
		
	}
	
	@Test(priority=6)
	public void Search_Functionality() throws Exception {
		type(Emp.Search,"tekys_16");
		Tables_Rows();
		Clear(Emp.Search);
	}
	
	@Test(priority=7)
	public void Search_Functionality_Name() throws Exception {
		type(Emp.Search,"shirisha");
		Tables_Rows();
		Clear(Emp.Search);
	}
	@Test(priority=8)
	public void Invalid_Search_Functionality() throws Exception {
		type(Emp.Search,"tekys16");
		Tables_Rows();
	}
}
