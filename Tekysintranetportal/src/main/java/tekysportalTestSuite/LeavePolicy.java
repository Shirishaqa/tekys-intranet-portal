package tekysportalTestSuite;

import java.io.BufferedInputStream;
import java.net.URL;

import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objects.Employees_Locators;
import objects.LeaveApproval_Locators;
import objects.LeaveBalance_Locators;
import objects.LeaveHistory_Locators;
import objects.LevaePolicy_Locators;
import objects.Registration;
import utils.BaseClass;

public class LeavePolicy extends BaseClass {

	
	Registration       Employee     = new Registration();
	WebLaunch          Webloagin    = new WebLaunch();
	Employees_Locators Emp          = new Employees_Locators();
	LeaveApproval_Locators  Approval   = new LeaveApproval_Locators();
	LeaveBalance_Locators   Balance            = new LeaveBalance_Locators();
	LeaveHistory_Locators history      = new LeaveHistory_Locators();
	LevaePolicy_Locators  Policy       = new     LevaePolicy_Locators();  
	

	
	@BeforeClass
	public void beforeclass() throws Exception {
		Webloagin.launch();
		
	}
	
	@Test
	public void Navigation() throws Exception {
	click(Policy.LeavePloicy);
	Thread.sleep(3000);
	URL TestURL = new URL("https://tekysportalqa.azurewebsites.net/assets/leavepolicy.pdf");
	BufferedInputStream bis = new BufferedInputStream(TestURL.openStream());
	PDDocument doc = PDDocument.load(bis);
	String text = new PDFTextStripper().getText(doc);
	doc.close();
	bis.close();
	System.out.println(text);
	Thread.sleep(3000);
	
	}
//	//@Test
//	public void Arrow() throws Exception {
//		click(Policy.Arrow);
//		Thread.sleep(2000);
//		click(Policy.Icon);
//		Thread.sleep(1000);
//		click()
//		
//	}
}
